<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;

/**
 * Description of AlumnoModel
 *
 * @author eneko
 */
class AlumnoModel extends Model{ //Si no esto no seria un modelo, sino una clase normal
    protected $table = 'alumnos'; //A que hacemos referencia
    protected $primaryKey = 'id'; //Siempre tiene que tener una primaryKey
    protected $returnType = 'object';
    //Estos campos son modificables y son los que añadimos para que recoja el formulario
    protected $allowedFields = ['NIF','nombre','apellido1','apellido2','nif','email'];
}
